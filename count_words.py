import os

print()
path = './txt/'
for filename in os.listdir(path):
    # print(filename)
    with open(f'{path}{filename}', 'r') as f:
        count = 0
        while line := f.readline():  # 這列動作： 1)讀取一行文字並存入line； 2)判斷line是否為空字串。
            print(line, end='')
            for char in line:
                if ord(char) >
                count += 1
            break
        print(count)
